Rails.application.routes.draw do
  root 'blogs#index'

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  resources :users do
    resources :blogs
  end
end
